<!-- omit in toc -->
# Peppy's Voron 2.4

> [FYSETC Voron R2 Pro Kit 350mm](https://www.fysetc.com/products/pre-sale-fysetc-newest-version-corexy-3d-printer-voron-2-4-r2-pro-with-hdmi5-display-cnc-hollow-gantry-high-quality-printer)

This is my 2nd printer build, started with an OG Ender 3 that I have done some
minor modifications to. After looking around, I chose the FYSETC kit b/c:

- Utilizes Canbus
- CNC tap
- Spider v3 Control Board
- 350mm Build size
- 5in Touchscreen
- Other minor upgrades
- Price :D

<!-- omit in toc -->
## Contents

- [Assembly](#assembly)
    - [Aftermarket Mods](#aftermarket-mods)
- [Electronics \& Klipper](#electronics--klipper)
    - [Aftermarket Electronics](#aftermarket-electronics)
    - [Klipper Plugins](#klipper-plugins)
- [Latest Input Shaping Results](#latest-input-shaping-results)
- [Links](#links)


## Assembly

Normal Voron 2.4 R2 instructions, but there are a few upgrades that were a little
hard to find docs on.

> NOTE: The PDF in the kit repo at the time of writing is the old 2.4 manual using
> afterburner. I asked them to update in discord but no such luck.

- Heatbed Insulation
    - I started off using this but ended up ripping it off. I'd try w/o it first
        - There wasn't much room for the Nevermore to breathe
        - It was taking too long to heat the chamber for ABS/ASA
- PEEK Isolation Columns
    - https://www.fysetc.com/products/fysetc-voron-peek-heated-bed-feet-isolation-column-hotbed-protector-cushion-damping-for-voron-2-4-r2-voron-trident-3d-printers
- 5in Touchscreen - https://www.youtube.com/watch?v=sUrsYsw5mUA
    - 4x M3 heat inserts
    - 14x M3x8 SHCS screws
- Lightweight Gantry - https://www.youtube.com/watch?v=u01iNdeB1rA
    - 8x M5 heat insert
    - 8x M5x12 BHCS
    - When assembling the x-gantry, go ahead and install all the bolts instead
      of waiting for the endstop
- Umbilical
    - https://mods.vorondesign.com/details/ho9WEyf6msbGKhTbtM59mQ
        - 4x M3x10 SHCS screws
        - 4x M3 heat inserts
    - The Y endstop plate mounts on the A drive housing
        - My kit **DID NOT** include 2x M3x35 screws that were needed
        - I used the included small self-tapping screws to mount the endstop
- TAP
    - The X endstop is mounted on the TAP (screws were inside tap packaging)
    - The assembly works mostly like the typical x-carriage
    - I found it easier to get the belts pinned first, before attaching to the rail
    - It's also easier to get the lower screws in place before placing against the rail,
      as the head does not fit through the screw holes
- Wiring
    - There is only a Z chain due to the CAN bus wiring
    - I found most of the wiring to be easy and straightforward
    - You don't need the cables sleeves, although I used one inside the Z drag chain
    - https://www.printables.com/model/142395-2020-extrusion-cable-clips-flush-and-guide
- Panels
    - I used non-standard panel moutns; see the below aftermark mods section for more info
    - I used 3mm foam tape on the top (originally an accident); only the back got 1mm foam tape

### Aftermarket Mods

- [Snap Latches](https://www.printables.com/model/775934-voron-24-snap-latches-metal-pins-and-cutting-guide)
    - On the front door, I cut the foam tape to fit around the latch
- [270 Degree Door Hinges](https://github.com/VoronDesign/VoronUsers/tree/main/printer_mods/chrisrgonzales/270_degree_hinge)
    -  I opted for these since they did not require adhesive/screws
    - The have been a litle loose, but otherwise great
- [Nevermore V6 Easy Lid](https://www.printables.com/model/514532-nevermore-v6-carbon-filter-easy-lid-remix)
    - I couldn't get the galouise door to work so I just re-pritned one of these
      guys. Much easier.  
- [Handles](https://mods.vorondesign.com/details/xa84lhUN5aMX4nmfZquaQ)
- [Bed Guide](https://www.aliexpress.us/item/3256806418426508.html)
    - I was missing those nubs on the prusa and these things rock
- [Titanium Extrusion Backers](https://www.aliexpress.us/item/3256806045424073.html)
    - People seem to like these, I didn't necessarily need them but impulse bought them
    - NOTE: The CNC hollow gantry DOES NOT work w/ the standard backer
- [Spool Relocator](https://www.printables.com/model/445960-voron-24-spool-holder-relocator)
    - My printer is too close to the wall to get a spool off the standard location
    - I have been happy with this so far but had to tweak the filament path a bit

## Electronics & Klipper

Components:

- Raspberry Pi 4
    - **Not included** in the kit
    - Flashed w/ Mainsail OS
- FYSETC Spider V3
    - This board is great and has plenty of ports for everything
    - This board has built-in CANbus support and can act as usb-can bridge
- FYSETC SB CAN Toolhead
    - This was one of the included upgrades that GREATLY simplifies wiring
    - The kit includes the "umbilical" cable that is the only thing that
      will run from the x-gantry to the electronics bay
- Nevermore v6
    - There are a few different ways to wire this up (WAGO, JST, etc)
    - I opted for the WAGO's after trying/failing to find an easy solution to
      make a 2-1 JST connector/cable
    - I did put a JST connector on the final wires; I plan on plugging the
      nevermore into an actual fan port rather than a 24V terminal
    - If the insulation was under the bed, the nevermore doesn't fit...

Setting it all up:

- We have two boards we need to flash (see the video below)
    - We need 24V power for this step; the SB TH board requires 24V
    - I used a bench power supply and the umbilical since the actual PSU was
      not setup yet
- Flashing Tutorial Video: https://www.youtube.com/watch?v=Cu1jGhjVP3g
- I opted to flash the Katapult bootloader on the SB Toolhead board so I could
  update it w/o requirig USB
    - NOTE: After flashing katapult, double tap the button to enter the bootloader,
      see the CAN UUID, and flash the initial klipper firmware
    - `dfu-util -d 0483:df11 -a 0 -R -D out/katapult.bin -s0x08000000:leave`
- The touchscreen had a few issues for me until I made the following adjustment
  in the pi's `/boot/config.txt` file

    ```
    # Enable DRM VC4 V3D driver
    #dtoverlay=vc4-kms-v3d  <--- original line; replaced w/ line below
    dtoverlay=vc4-fkms-v3d
    ```
- The docs aren't really clear on the best way to orient the fans
    - I've found that an exhaust on the "right" (PSU side) works well for me
    - I ended up w/ some lower speed intake on the other side

### Aftermarket Electronics

- I added "daylight on a stick" to the top rails and wired it down to a 24V pwm
  fan port on the spider board w/ a JST connector
    - https://www.aliexpress.us/item/3256805990745720.html
- I added two temperature probes to collect chamber temps, woo!
    - https://www.amazon.com/gp/product/B07F3SMRLJ/'
    - There were a few empty ports on the V3 board for thermistors
    - I just have them tucked in extrusions at the moment
    - The upper chamber temp rides the XY gantry
- I upgraded the electronics fans to [noctuas](https://www.amazon.com/gp/product/B009LEKGGE)
    - Got them for $14 a piece at the time
    - Used some custom cabling to run them from a single fan port at 12V
    - Moved the old gdstime fans to the other side as intake and throttled them
      way down to be less noisy

### Klipper Plugins

- [Timelapse](https://github.com/mainsail-crew/moonraker-timelapse)
    - Builtin to Mainsail
    - Cameras coming soon...
- [Spoolman](https://github.com/Donkie/Spoolman)
    - Builtin moonraker support
    - Track spool usage
- [KAMP](https://github.com/kyleisah/Klipper-Adaptive-Meshing-Purging)
    - Adds a purge line close to the print area; I don't have to mess with one
    - I don't use the adaptive mesh leveling per-print, my mesh is pretty consistent
    - Manual cloned install
    - TODO: Klipper has adaptive mesh now?
- [Mobileraker Companion](https://github.com/Clon1998/mobileraker_companion)
    - Used KIAUH to install
    - Useful to send push notifications to my phone/tablet
- [LED Effects](https://github.com/julianschill/klipper-led_effect)
    - Required for the Stealthburner LEDs
    - Installed at some point for the FYSETC `stealthburner_led_effects_barf.cfg` file

## Latest Input Shaping Results

![](./input_shaping/shaper_calibrate_x.png)
![](./input_shaping/shaper_calibrate_y.png)

## Links

- [FYSETC Kit Github](https://github.com/FYSETC/FYSETC-VORON-2.4-R2-Pro)
- [FYSETC Spider 3.0 Github](https://github.com/FYSETC/FYSETC-SPIDER/tree/main/hardware/V3.0)
- [FYSETC SB CAN Toolhead Github](https://github.com/FYSETC/FYSETC_SB_CAN_TOOLHEAD/tree/main)
- [Klipper CANBUS Docs](https://www.klipper3d.org/CANBUS.html)
