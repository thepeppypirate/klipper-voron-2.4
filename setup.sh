#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

KLIPPER_CONFIG_DIR="${KLIPPER_CONFIG_DIR:-$HOME/printer_data/config}"

for filepath in $THIS_DIR/*.cfg; do
    filename=$(basename $filepath)
    echo "Installing: $filename"
    ln -sf "${filepath}" "${KLIPPER_CONFIG_DIR}/${filename}"
done
